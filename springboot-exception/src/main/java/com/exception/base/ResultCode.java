package com.exception.base;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @desc:
 * @author: rainscloud
 * @date: 2021/8/11
 */
@NoArgsConstructor
@AllArgsConstructor
public enum ResultCode {

    SUCCESS(200, "请求成功"),
    FAILED(500, "服务器错误");

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

