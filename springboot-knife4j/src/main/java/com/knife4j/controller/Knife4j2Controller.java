package com.knife4j.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.knife4j.vo.ReqVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @desc: knife4j使用
 * @author: rainscloud
 * @date: 2021/8/12
 */
@Api(tags = "knife4j2")
@RestController("/knife4j2")
public class Knife4j2Controller {

    @ApiImplicitParam(name = "id", value = "编号", required = true)
    @ApiOperation(value = "methodA方法", notes = "aa", httpMethod = "GET")
    @ApiOperationSupport(author = "rainscloud", order = 2)
    @GetMapping("/methodC")
    public String methodA(@RequestParam("id") String id) {
        return id;
    }

    @PostMapping("/methodD")
    @ApiOperationSupport(author = "rainscloud", order = 1)
    public String methodB(@RequestBody ReqVo reqVo) {
        return reqVo.getId();
    }
}
