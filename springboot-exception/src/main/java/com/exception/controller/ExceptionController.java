package com.exception.controller;

import com.exception.base.R;
import com.exception.exception.CustomException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @desc: ExceptionController
 * @author: rains cloud
 * @date: 2021/8/10
 */
@RestController
public class ExceptionController {

    @GetMapping("exceptionA")
    public void methodA() {
        int a = 100 / 0;
    }

    @GetMapping("exceptionB")
    public void methodB() {
        List list = new ArrayList<>();
        System.out.println(list.get(0));
    }

    @GetMapping("exceptionA1")
    public R methodA1() {
        int a = 100 / 0;
        return R.success();
    }

    @GetMapping("exceptionB1")
    public void methodB1() {
        List list = new ArrayList<>();
        System.out.println(list.get(0));
    }

    @GetMapping("exceptionC")
    public void methodC() throws CustomException {
        int a = 1;
        if (a == 1) {
            throw new CustomException(10086, "自定义异常");
        }
    }

    /**
     * 局部异常处理
     */
    //@ExceptionHandler(Exception.class)
    //public String exHandler(Exception e) {
    //    // 判断发生异常的类型是除0异常则做出响应
    //    if (e instanceof ArithmeticException) {
    //        return "发生了除0异常";
    //    }
    //    // 未知的异常做出响应
    //    return "发生了未知异常";
    //}
}
