package com.exception.exception;


import com.exception.base.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @desc: 全局异常处理
 * @author: rainscloud
 * @date: 2021/8/11
 */
@RestControllerAdvice
public class DefaultException {

    //@ExceptionHandler({NullPointerException.class})
    //public String exception(NullPointerException exception) {
    //    return "空指针异常";
    //
    //}
    //
    //@ExceptionHandler({IndexOutOfBoundsException.class})
    //public String exception(IndexOutOfBoundsException exception) {
    //    return "数组越界异常";
    //}
    //
    //@ExceptionHandler({Exception.class})
    //public String exception(Exception exception) {
    //    return "服务器错误";
    //}

    @ExceptionHandler({NullPointerException.class})
    public String exception(NullPointerException exception) {
        return "空指针异常";

    }

    @ExceptionHandler({IndexOutOfBoundsException.class})
    public String exception(IndexOutOfBoundsException exception) {
        return "数组越界异常";
    }

    @ExceptionHandler({CustomException.class})
    public R exception(CustomException e) {
        return R.failed(e.getCode(),e.getMessage());
    }

    @ExceptionHandler({Exception.class})
    public R exception(Exception e) {
        return R.failed(e.getMessage());
    }
}
