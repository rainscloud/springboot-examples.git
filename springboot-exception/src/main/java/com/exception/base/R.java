package com.exception.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @desc: 统一返回对象
 * @author: rainscloud
 * @date: 2021/8/11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class R<T> {
    /**
     * 返回状态码
     */
    private Integer code;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 数据
     */
    private T data;

    public static R success() {
        return new R(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), null);
    }

    public static R success(Object data) {
        return new R(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), data);
    }

    public static R failed() {
        return new R(ResultCode.FAILED.getCode(), ResultCode.FAILED.getMsg(), null);
    }

    public static R failed(String msg) {
        return new R(ResultCode.FAILED.getCode(), msg, null);
    }

    public static R failed(int code, String msg) {
        return new R(code, msg, null);
    }
}
