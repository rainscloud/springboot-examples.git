package com.exception.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @desc: 自定义异常
 * @author: rainscloud
 * @date: 2021/8/11
 */

@Data
@NoArgsConstructor
public class CustomException extends Exception {

    private static final long serialVersionUID = 1L;

    private Integer code;

    private String mes;

    /**
     * @param code 状态码
     * @param msg  异常返回信息
     * @description 构造器
     */
    public CustomException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

}
