package com.knife4j.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc: 请求参数Vo
 * @author: rainscloud
 * @date: 2021/8/12
 */
@Data
public class ReqVo {
    @ApiModelProperty()
    private String id;
    @ApiModelProperty()
    private String name;
}
